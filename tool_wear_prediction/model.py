import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import KFold
import keras_tuner as kt
import os


tf.keras.backend.set_epsilon(1)

class myHyperModel(kt.HyperModel):
    def __init__(self, input_shape:tuple):
        self.input_shape = input_shape

    def build(self,hp) : 
        """
        
        Creates a hypermodel for hyperparameter tuning with Keras Tuner

        """
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input(shape=self.input_shape))
        
        hp_units = hp.Int('units', min_value=32, max_value=512, step=32)
        hp_layers = hp.Int('n_layers',min_value=1, max_value=5, step=1)
        hp_learning_rate = hp.Choice('learning_rate', values=[1e-3, 3e-3, 6e-3 ,1e-4, 5e-4])
        hp_loss_metric = hp.Choice('loss_metric',values=['mse','mae'])

        for i in range(hp_layers):
            model.add(tf.keras.layers.Dense(units=hp_units, activation='relu',  kernel_initializer='he_uniform'))

        model.add(tf.keras.layers.Dense(1, activation = 'linear'))

        optimizer = tf.keras.optimizers.Adam(learning_rate=hp_learning_rate)
        model.compile(optimizer=optimizer, loss=hp_loss_metric, metrics=['mae','mape'])

        return model


def read_split_data(path: str, dropcols = ['case']):
    """
    This function performs the following actions:

    1) Reads the (preprocessed) data in .csv format

    2) Normalizes the input values (min-max)

    3) Splits the data to a training and a test set

    """

    data = pd.read_csv(path)
    data = data.drop(columns=dropcols)
    input_columns = [col for col in data.columns if col != "VB"]

    for col in input_columns:
        col_min = data[col].min()
        col_max = data[col].max()
        data[col] = (data[col] - col_min) / (col_max - col_min)

    data = data[[col for col in data.columns if col != 'VB'] + ['VB']] # move VB at the end so that we can use -1 index

    return data, input_columns


def tune_hyperparams(data:pd.DataFrame, epochs:int, batch_size=32, validation_split=None):
    """
    
    Performs hyperparameter tuning using the Keras Tuner

    """
    
    if not os.path.exists("hyperparam_tuning"): 
        os.makedirs("hyperparam_tuning") 

    input_columns = [col for col in data.columns if col != "VB"]
    train, test = train_test_split(data, test_size=0.2, random_state=13, shuffle=True)
    x_train = train.loc[:, input_columns]
    y_train = train.loc[:, "VB"]
    x_test = test.loc[:, input_columns]
    y_test = test.loc[:, "VB"]

    my_hyper_model = myHyperModel((x_train.shape[1],))

    tuner = kt.Hyperband(my_hyper_model,
                         max_epochs=200,
                         objective='val_mae',
                         directory='hyperparam_tuning',
                         project_name='hyerparam_hyperband')
    
    tuner.search(x_train, y_train,epochs=epochs,validation_split=validation_split,batch_size=batch_size)

    # Get the optimal hyperparameters
    best_hps=tuner.get_best_hyperparameters(num_trials=1)[0]

    print(f"""
    The hyperparameter search is complete. The optimal number of units in the first densely-connected
    layer is {best_hps.get('units')}, the number of hidden layers is {best_hps.get('n_layers')}, 
    the optimal learning rate for the optimizer is {best_hps.get('learning_rate')},
    and the optimal loss metric is {best_hps.get('loss_metric')}.
    """)

    best_model = tuner.hypermodel.build(best_hps)
    best_model.fit(x_train,y_train,epochs=200,validation_split=0.2)

    best_model.evaluate(x_test,y_test)

    return best_hps

def create_model(x_train: pd.DataFrame) -> tf.keras.models.Sequential:
    """

    Create the neural network

    """
    
    model = tf.keras.models.Sequential()
    model.add(tf.keras.Input(shape=(x_train.shape[1],)))
    model.add(tf.keras.layers.Dense(160, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dense(160, activation='relu', kernel_initializer='he_uniform'))
    model.add(tf.keras.layers.Dense(1, activation = 'linear'))
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.001)
    model.compile(optimizer=optimizer, loss='mse', metrics=['mae', 'mape'])

    return model

def train_cross_validate(data: pd.DataFrame, n_folds: int, plot=False, verbose=False):
    """

    Performs a k-fold validation process on the provided model architecture

    """

    kfold = KFold(n_splits=n_folds, shuffle=True)
    fold_num = 0 
    eval_metrics = []

    for train, test in kfold.split(data):
        x_train = data.iloc[train,0:-2]
        y_train = data.iloc[train,-1]
        x_test = data.iloc[test,0:-2]
        y_test = data.iloc[test,-1]
        
        model = create_model(x_train)
        
        history = model.fit(x_train, y_train, epochs = 200, batch_size=8, validation_split=0.20, verbose=False)  

        eval_metrics.append(model.evaluate(x_test, y_test, verbose=False))
        hist = pd.DataFrame(history.history)

        if plot :
            plot_loss(history, "results/loss_fold_%d.png"%fold_num)

        fold_num += 1 

    eval_metrics = np.array(eval_metrics)
    eval_metrics_mean = np.mean(eval_metrics, axis=0)

    if verbose :
        print("Mean loss : %lf -- Mean mae : %lf -- Mean mape : %lf"%(eval_metrics_mean[0],eval_metrics_mean[1],eval_metrics_mean[2]))
    
    return eval_metrics_mean

def correlation_analysis(data: pd.DataFrame, n_folds: int, dropcols=None, plot_corr=False, verbose=True):
    """

    This function performs the following actions:

    1) Calculated the Kendall's correlation factor

    2) Evaluates the NN using k-fold validation

    It provides the option for the user to give a list of columns to be dropped.
    This list must contain the columns in the order the user wants them to be dropped.

    """

    # Kendal's correlation factor better suits nonlinear, nongaussian, noncontinuous data
    corr = data.corr(method='kendall')

    if plot_corr : 
        plt.figure(figsize=(10,11))
        sns.heatmap(corr, cmap="Greens", annot=True,fmt=".3f")
        plt.title("Kendall's correlation factor")
        plt.savefig("results/kendall.png", format='png', dpi=600)

    eval_metrics = []
    corr['VB'] = abs(corr['VB'])
    dropcols_sorted = corr.sort_values(by='VB')['VB'].index.tolist()
    dropcols_sorted = dropcols_sorted[0:-1] # do not drop 'VB' and the most signifficant factor

    if dropcols!=None : 
        dropcols_sorted = dropcols

    for i in range(len(dropcols_sorted)) : 
        data = data.drop(columns=dropcols_sorted[i])
        if verbose:
            print("=========================")
            print("Excluded data : ",dropcols_sorted[0:i])
        eval_cv = train_cross_validate(data,n_folds, verbose=verbose)
        eval_metrics.append(eval_cv)
    
    return eval_metrics

def plot_loss(history, path: str):
    """

    Plot the model loss evolution

    """
    plt.figure()
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.title("MSE vs Epoch")
    plt.savefig(path)


if __name__ == "__main__":
    
    n_folds = 10

    data,input_columns = read_split_data("data/train_data.csv", dropcols=['case'])

    # droplist_lit = ['vib_table','vib_spindle','AE_spindle','smcDC_rms','cast_iron','steel','DOC','time','run']
    # correlation_analysis(data,n_folds,verbose=True, plot_corr=True)
    # correlation_analysis(data,n_folds,dropcols=droplist_lit,verbose = True)
    train_cross_validate(data,n_folds,verbose=True, plot=True)

    # tune_hyperparams(data,epochs=200,batch_size=8,validation_split=0.2)
    
