# Tool wear prediction in milling using Neural Networks 
## Steps to use the codes in this project : 
* Create a directory named "data" and add the mill.mat file in there
* Create an empty directory named "results"
* Run the read_mat.py file
* Run the preprocess.py file 
* Everything is ready to run the codes on model.py 

## Extracting data from the .mat file to .csv
* The read_mat.py script will extract all the data
from the mill.mat file and place them in different .csv files. This is done using the HDF module for Python.
* The data corresponding to the SCALAR_KEYS (found in keys.py) will all be saved in a .csv file, named scalar_data.csv and placed inside of the "data" directory. 
* The data corresponding to TIMESERIES_KEYS will each have a seperate .csv file created inside of the same directory. 
* Only timeseries with 9000 entries are kept, and the rest dropped.

## Preprocessing the data
* By running preprocess.py a new .csv file (ouput_data.csv) is created inside of the "data" directory.
* This file contains all the data from scalar_data.csv, but is augmented with the RMS of each timeseries.
* The material encoding is converted from integer encoding (1 for cast-iron and 2 for steel), to one-hot encoding.
* All missing values are dropped from the dataset.

## Using the various experiments in model.py 
### Training and cross-validation
* Just uncomment the line which runs the function "train_cross_validate". This function calls the "create_model" function with creates the ANN based on the optimal structure that was determined in the study. 

### Running a correlation analysis
* If a selected importance order is desired, create a list with the columns of the data in the desired order to be dropped.
Remember to include one more element in this list, which should remain after all others are removed (this is because of the way the code works).
* Otherwise, the default "correlation_analysis" function will calculate the Kendall's correlation factor and drop the less important columns one by one.

### Tuning the hyperparameters and structure
* Just run the function "tune_hyperparams"