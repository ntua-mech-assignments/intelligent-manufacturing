from typing import Dict, Callable

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keys import TIMESERIES_KEYS

def rms_filter(timeseries: np.ndarray, window_start: int = 0, window_end: int = -1) -> float:
    """
    
    Compute the RMS from a timeseries, or a slice of it

    """

    rms = np.sqrt(np.mean(timeseries[window_start: window_end])**2)

    return rms

def fft_filter(timeseries: np.ndarray, sampling_freq: float = 250, window_start: int = 0, window_end: int = -1) -> float:
    """
    
    Compute the frequency corresponding to the maximum value of the PSD
    
    (Experimental)

    """
    
    f = timeseries[window_start:window_end]

    f = f - np.mean(f)
    
    fhat = np.fft.fft(f)
    
    psd = fhat * np.conjugate(fhat)

    freq = np.fft.fftfreq(len(f), 1/sampling_freq)

    return freq[np.argmax(psd)]
    
def timeseries_to_scalar(dataframe: pd.DataFrame, filters: Dict[str, Callable[[np.ndarray, int, int], float]]) -> pd.DataFrame:
    """
    
    Extract scalar values for all timeseries

    """

    for key in TIMESERIES_KEYS:

        timeseries = pd.read_csv(f"data/{key}.csv")

        scalar_features = {feature_key: [] for feature_key in filters}

        for col in timeseries.columns:
            
            for feature_key, feature_filter in filters.items():

                scalar_features[feature_key].append(feature_filter(timeseries[col]))

        for feature_key, feature_value in scalar_features.items():

            dataframe[f"{key}_{feature_key}"] = np.array(feature_value)
    
    return dataframe

def one_hot_encode_material(dataframe: pd.DataFrame, encoding: Dict[int, str]) -> pd.DataFrame:
    """
    
    Convert the integer material encoding (i.e 2 for steel) to one-hot encoding (i.e [0, 1])

    """
    
    for value in encoding.values():

        dataframe[value] = np.zeros_like(dataframe["material"])

    for i in range(len(dataframe["material"])):
            
        material_type_int = int(dataframe["material"][i])
            
        material_type_str = encoding[material_type_int]

        dataframe[material_type_str][i] = 1.0 
    
    dataframe = dataframe.drop(columns = "material")
    
    return dataframe


if __name__ == "__main__":

    data = pd.read_csv("data/scalar_data.csv")

    filters = {"rms": lambda timeseries: rms_filter(timeseries, 3000, 6000)}
               #"fft": lambda timeseries: fft_filter(timeseries, 250, 3000, 6000)}
    
    data = timeseries_to_scalar(data, filters)

    data = one_hot_encode_material(data, {1: "cast_iron", 2: "steel"})

    data = data.dropna()

    data.to_csv("data/train_data.csv", index=False)