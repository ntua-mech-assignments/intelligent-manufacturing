SCALAR_KEYS = ['case', 
                'run', 
                'time',
                'DOC',
                'feed',
                'material',
                'VB']

TIMESERIES_KEYS = ['smcDC',
                    'vib_table',
                    'vib_spindle',
                    'AE_table',
                    'AE_spindle']