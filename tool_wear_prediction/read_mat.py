import h5py
import pandas as pd
import numpy as np

from keys import SCALAR_KEYS, TIMESERIES_KEYS

def read_mat_file(path: str) -> None:
    """
    Convert the .mat file to multiple .csv files, 
    using the HDF interface
    """
    with h5py.File(path, "r") as mat_file:

        mill_data = mat_file["mill"]

        idxs_to_skip = []

        for key in TIMESERIES_KEYS:
            
            timeseries_data = []

            idx = 0

            for ref in mill_data[key]:

                timeseries_data.append(mill_data[ref[0]][0])

                if timeseries_data[-1].shape[0] != 9000:

                    timeseries_data.pop()

                    idxs_to_skip.append(idx)
                
                idx += 1 

            timeseries_data = pd.DataFrame(np.array(timeseries_data).T)

            timeseries_data.to_csv(f"data/{key}.csv", index=False)

        scalar_data = {}

        for key in SCALAR_KEYS:

            scalar_data[key] = []

            idx = 0

            for ref in mill_data[key]:
                
                if idx not in idxs_to_skip:

                    scalar_data[key].append(mill_data[ref[0]][0][0])
                
                idx += 1 
        
        scalar_data = pd.DataFrame.from_dict(scalar_data)

        scalar_data.to_csv("data/scalar_data.csv", index=False, na_rep="NaN")

if __name__ == "__main__":
    
    read_mat_file("data/mill.mat")
        