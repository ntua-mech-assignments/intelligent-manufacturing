clearvars
clc

%% machining data handbook plots
load('material_handbook_data.mat')

Data2eval = cell(3,4,3); %DOC, TOOL, MATERIAL
Data2eval{1,1,1} = material_1_DOC_1mm_tool_1_machining_handbook;
Data2eval{1,2,1} = material_1_DOC_1mm_tool_2_machining_handbook;
Data2eval{1,3,1} = material_1_DOC_1mm_tool_3_machining_handbook;
Data2eval{1,4,1} = material_1_DOC_1mm_tool_4_machining_handbook;
Data2eval{2,1,1} = material_1_DOC_4mm_tool_1_machining_handbook;
Data2eval{2,2,1} = material_1_DOC_4mm_tool_2_machining_handbook;
Data2eval{2,3,1} = material_1_DOC_4mm_tool_3_machining_handbook;
Data2eval{2,4,1} = material_1_DOC_4mm_tool_4_machining_handbook;
Data2eval{3,1,1} = material_1_DOC_8mm_tool_1_machining_handbook;
Data2eval{3,2,1} = material_1_DOC_8mm_tool_2_machining_handbook;
Data2eval{3,3,1} = material_1_DOC_8mm_tool_3_machining_handbook;
Data2eval{3,4,1} = material_1_DOC_8mm_tool_4_machining_handbook;

Data2eval{1,1,2} = material_2_DOC_1mm_tool_1_machining_handbook;
Data2eval{1,2,2} = material_2_DOC_1mm_tool_2_machining_handbook;
Data2eval{1,3,2} = material_2_DOC_1mm_tool_3_machining_handbook;
Data2eval{1,4,2} = material_2_DOC_1mm_tool_4_machining_handbook;
Data2eval{2,1,2} = material_2_DOC_4mm_tool_1_machining_handbook;
Data2eval{2,2,2} = material_2_DOC_4mm_tool_2_machining_handbook;
Data2eval{2,3,2} = material_2_DOC_4mm_tool_3_machining_handbook;
Data2eval{2,4,2} = material_2_DOC_4mm_tool_4_machining_handbook;
Data2eval{3,1,2} = material_2_DOC_8mm_tool_1_machining_handbook;
Data2eval{3,2,2} = material_2_DOC_8mm_tool_2_machining_handbook;
Data2eval{3,3,2} = material_2_DOC_8mm_tool_3_machining_handbook;
Data2eval{3,4,2} = material_2_DOC_8mm_tool_4_machining_handbook;

Data2eval{1,1,3} = material_3_DOC_1mm_tool_1_machining_handbook;
Data2eval{1,2,3} = material_3_DOC_1mm_tool_2_machining_handbook;
Data2eval{1,3,3} = material_3_DOC_1mm_tool_3_machining_handbook;
Data2eval{1,4,3} = material_3_DOC_1mm_tool_4_machining_handbook;
Data2eval{2,1,3} = material_3_DOC_4mm_tool_1_machining_handbook;
Data2eval{2,2,3} = material_3_DOC_4mm_tool_2_machining_handbook;
Data2eval{2,3,3} = material_3_DOC_4mm_tool_3_machining_handbook;
Data2eval{2,4,3} = material_3_DOC_4mm_tool_4_machining_handbook;
Data2eval{3,1,3} = material_3_DOC_8mm_tool_1_machining_handbook;
Data2eval{3,2,3} = material_3_DOC_8mm_tool_2_machining_handbook;
Data2eval{3,3,3} = material_3_DOC_8mm_tool_3_machining_handbook;
Data2eval{3,4,3} = material_3_DOC_8mm_tool_4_machining_handbook;

%% import fuzzy inferrence system
fis = readfis('paper_copy');

%% import machining data handbook values
mchbdata = cell2mat(readcell('mchbval.xlsx','sheet','Sheet1','range','C3:N14'));

%% Material 2 adjustment scheme
mchbdatafix = mchbdata; %separate data table to hold adjusted values
adj(4,3) = zeros; %Adjustment value table
m = 2; %Material 2 selected
for t=1:4 %4 tool types for material 2
    maxofminspd = max(mchbdata((t-1)*3+1:(t-1)*3+3,(m-1)*4+1)); %max of the minimum speed of ranges for material and tool type
    for d=1:3 %3 DOC per tool type
        minspd = mchbdata((t-1)*3+d,(m-1)*4+1); %minimum speed of speed range for selection
        adj(t,d) = maxofminspd-minspd; %speed adjustment value
        mchbdatafix((t-1)*3+d,(m-1)*4+1) = mchbdata((t-1)*3+d,(m-1)*4+1) + adj(t,d); %minimum speed of speed range for selection
        mchbdatafix((t-1)*3+d,(m-1)*4+2) = mchbdata((t-1)*3+d,(m-1)*4+2) + adj(t,d); %maximum speed of speed range for selection
    end
end

%% evaluate FIS
DOCvalues = [1,4,8];
error = zeros(36,1);
e = 1;
var = 1;
for m=1:3 %3 materials
    f = figure;
    f.WindowState = 'maximized';
    for t=1:4 %4 tool types per material
        subplot(2, 2, t);
        ylabel('Cutting speed m/min','fontsize',14)
        xlabel('Material hardness BHN','fontsize',14)
        title("Material type "+ m +" Tool type "+ t +"")
        hold on
        %minimum and maximum speeds per material and tool combination
        if (m==2) %selecting adjusted value table for inputs for material 2
            totalminspd = min(mchbdatafix((t-1)*3+1:(t-1)*3+3,(m-1)*4+1));
            totalmaxspd = max(mchbdatafix((t-1)*3+1:(t-1)*3+3,(m-1)*4+2));
        else
            totalminspd = min(mchbdata((t-1)*3+1:(t-1)*3+3,(m-1)*4+1));
            totalmaxspd = max(mchbdata((t-1)*3+1:(t-1)*3+3,(m-1)*4+2));
        end
        totalspdrange = totalmaxspd - totalminspd;
        for d=1:3 %3 DOC per tool type and material
            % basic values
            doc = DOCvalues(d); %depth of cut input
            minspd = mchbdata((t-1)*3+d,(m-1)*4+1); %minimum speed of speed range for selection
            maxspd = mchbdata((t-1)*3+d,(m-1)*4+2); %maximum speed of speed range for selection
            minhrd = mchbdata((t-1)*3+d,(m-1)*4+3); %minimum hardness of hardness range for selection
            maxhrd = mchbdata((t-1)*3+d,(m-1)*4+4); %maximum hardness of hardness range for selection
            hrdrange = maxhrd - minhrd;
            output = zeros(1,hrdrange+1);
            spdfis = zeros(1,hrdrange+1);
            hrd = Data2eval{d,t,m}(:,1); %hardness plot
            spd = Data2eval{d,t,m}(:,2); %speed plot
            hrdfis = linspace(minhrd,maxhrd,hrdrange+1); %hardness plot for fis
            % Initialize interpolated_spd with NaN
            interpolated_spd = NaN(size(hrdfis));
            % Interpolate spd values between discrete points
            for i = 1:length(hrd)-1
                idx_range = hrdfis >= hrd(i) & hrdfis <= hrd(i+1);
                interpolated_spd(idx_range) = linspace(spd(i), spd(i+1), sum(idx_range));
            end
            % Fill the extrapolated values with the last known value
            interpolated_spd(isnan(interpolated_spd)) =  spd(end); 
            %plot type alternator
            if (d==1)
                h1 = plot(hrdfis,interpolated_spd,"color","#D95319","LineStyle","-.", LineWidth=1);
            elseif (d==2)
                h1 = plot(hrdfis,interpolated_spd,"color","#7E2F8E","LineStyle","-.",LineWidth=1);
            else
                h1 = plot(hrdfis,interpolated_spd,"color","#4DBEEE","LineStyle","-.",LineWidth=1);
            end
            temp = linspace(0,20,hrdrange+1);
            %FIS output
            for k=1:hrdrange+1
                output(1,k) = evalfis(fis,[(temp(1,k)) doc]); %FIS output for depth of cut for various hardnesses
                spdfis(1,k) = output(1,k)*totalspdrange/10 + totalminspd; %FIS output transformation according to paper
                %material 2 output adjustment
                if (m==2)
                    spdfis(1,k) = spdfis(1,k) - adj(t,d);
                end
            end
            plot(hrdfis,spdfis,LineWidth=1.5)
            %error testing
            error(e,1) =  mae(interpolated_spd,spdfis);
            txt = sprintf('MAE: %.2f m/min', error(e, 1));
            label(h1,txt)
            e = e + 1;
        end
        legend("Reference 1 mm","FIS 1 mm", "Reference 4 mm", "FIS 4 mm", "Reference 8 mm", "FIS 8 mm")
        set(gca,'XLim',[minhrd,maxhrd]);
    end
        path =  'figures/material' + string(m) + '.png';
        saveas(f,path);
        var = var + 1;
        hold off
end
disp("MAE per input combination")
disp(error)
disp("MAE average")
disp(mean(error))
