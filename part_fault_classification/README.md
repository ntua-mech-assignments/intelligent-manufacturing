# Part fault classification
This folder contains code that utilizes machine vision and Neural Networks to perform fault classification on parts produced with hot-rolling.

## Usage 
* Create a folder named "data" and extract the dataset images there in 4 separate folders ("In", "Pa", "PS", "Sc")
* Run all codes from the `part_fault_classification` directory