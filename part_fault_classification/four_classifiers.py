# In[0]

from os.path import join
from typing import Dict, Tuple, List

from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.model_selection import train_test_split


def read_features_and_labels(path: str, class_name: str) -> Tuple[np.ndarray]:
    """
    
    Read the .csv files corresponding to the training and testing
    features and labels

    """
    filename = "%s_features_training.csv"%class_name
    x_train = pd.read_csv(
        join(path, filename), delimiter=",", header=None, dtype="float64"

    )
    x_test = pd.read_csv(
        join(path, "features_testing.csv"), delimiter=",", header=None, dtype="float64"

    )
    filename = "%s_labels_training.csv"%class_name
    y_train = pd.read_csv(
        join(path, filename), delimiter=",", header=None, dtype="string"
    )
    y_test = pd.read_csv(
        join(path, "labels_testing.csv"), delimiter=",", header=None, dtype="string"
    )
    return x_train, x_test, y_train, y_test


def labels_to_int(labels: pd.DataFrame, mapping: Dict[str, int] = None) -> np.ndarray:
    """

    Convert the labels from strings to ints

    """
    labels_int = []
    for i in range(len(labels[0])):
        labels_int.append(mapping[labels[0][i]])
    return np.array(labels_int)


def normalize_features(features: pd.DataFrame) -> np.ndarray:
    """
    
    Normalize all features (min-max)

    """
    for col in features.columns:
        col_min = features[col].min()
        col_max = features[col].max()
        features[col] = (features[col] - col_min) / (col_max - col_min)
    return features.to_numpy()


def labels_to_binary(labels: np.ndarray, label: int) -> np.ndarray:
    """

    Binarize the labels for a single image class
    
    """
    labels = np.where(labels == label, np.ones_like(labels), np.zeros_like(labels))
    return labels


def create_model(
    n_input: int, n_output: int, neurons_per_layer: List[int] = [64]
) -> tf.keras.Sequential:
    """

    Build a single network

    """
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(n_input,)))
    for n_neurons in neurons_per_layer:
        model.add(
            tf.keras.layers.Dense(
                n_neurons, activation="relu", kernel_initializer="he_uniform"
            ),
        )
    model.add(tf.keras.layers.Dense(n_output, activation="sigmoid"))
    return model


def train_model(
    model: tf.keras.Sequential,
    x_train: np.ndarray,
    y_train: np.ndarray,
    x_test: np.ndarray = None,
    y_test: np.ndarray = None,
) -> tf.keras.Sequential:
    """
    
    Train the network

    """
    optimizer = tf.keras.optimizers.Adam(learning_rate=0.005)

    model.compile(
        optimizer=optimizer,
        loss="binary_crossentropy",
        metrics=["accuracy"],
    )

    model.fit(
        x_train, y_train, epochs=100, batch_size=16, validation_split=0.15, shuffle=True
    )

    if x_train is not None and y_train is not None:
        model.evaluate(x_test, y_test)
    return model


def classify_features(x:np.ndarray, classifiers: Dict[str, tf.keras.Sequential], mapping: Dict[str, int], in_prob: float, in_model_prob: float) -> np.ndarray : 
    # probs is a matrix containing the predictions of the 3 classifiers
    # each column contains the probability that the image belongs to a class
    # each row represents a different feature vector (i.e. a different image)
    probs = np.zeros((x.shape[0], len(classifiers)))
 
    # y_pred contains the predicted labels
    # it is 1 x n_test_length in order to be used directly with the confusion matrix function
    y_pred = ["" for _ in range(probs.shape[0])]
    # mapping_inv = {v: k for k, v in mapping.items()} # inverse mapping (int to label)


    for k, v in classifiers.items():
        if k!="In":
            probs[:, mapping[k]] = (v.predict(x)).flatten()
    
    in_model_pred = classifiers["In"].predict(x) # probability predicted by the In model
    
    arg_max_prob = np.argmax(probs, axis=1)
    max_prob = np.max(probs, axis=1)

    for i in range(probs.shape[0]): 
        
        # both the In model must be sure and the others unsure to consider an image as In
        if max_prob[i] < in_prob and in_model_pred[i] > in_model_prob:
            y_pred[i] = mapping["In"]
        else : 
            y_pred[i] = arg_max_prob[i]


    return y_pred


def test_multi_classifier_model(
    x_test: np.ndarray,
    y_test: np.ndarray,
    mapping: Dict[str, int],
    classifiers: Dict[str, tf.keras.Sequential],
    in_prob: float,
    in_model_prob: float
):

    y_pred = classify_features(x_test, classifiers, mapping, in_prob, in_model_prob)
    return y_pred

#################################################################################################################
#################################################################################################################
# In[1]
# Read the features and corresponding labels
# Normalize each feature
# Convert the labels from strings to ints
# path = "part_fault_classification/"
path = "" # when running from MATLAB
mapping = {"Pa": 0, "PS": 1, "Sc": 2, "In": 3}
in_prob = 0.7 # lowering this value essentially corrects the last row of the confusion matrix
in_model_prob = 0.65 # increasing this value essentially corrects the last collumn of the confusion matrix

# x_train, x_test, y_train, y_test = read_features_and_labels(path)
# x_train, x_test = normalize_features(x_train), normalize_features(x_test)
# y_train, y_test = labels_to_int(y_train, mapping), labels_to_int(y_test, mapping)

# Binarize the labels for each class (except Sc)
# Create and train a binary classifier for each class (except Sc)
y_train_binary, y_test_binary = {}, {}
classifiers = {}
for k, v in mapping.items():
    # if k != "In":
    x_train, x_test, y_train, y_test = read_features_and_labels(path, k)
    x_train, x_test = normalize_features(x_train), normalize_features(x_test)
    y_train, y_test = labels_to_int(y_train, mapping), labels_to_int(y_test, mapping)    

    y_train_binary[k] = labels_to_binary(y_train, v)
    y_test_binary[k] = labels_to_binary(y_test, v)
    model = create_model(len(x_train[0]), 1, [64])
    model = train_model(model, x_train, y_train_binary[k], x_test, y_test_binary[k])
    classifiers[k] = model

#################################################################################################################
#################################################################################################################
# In[2]
# Test the performance
y_pred = test_multi_classifier_model(x_test, y_test, mapping, classifiers, in_prob, in_model_prob)
confusion = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusion_matrix=confusion)
accuracy = np.sum(np.diag(confusion)) / len(y_pred)
print("Multi network accuracy = %lf"%accuracy)
disp.plot()
plt.show()