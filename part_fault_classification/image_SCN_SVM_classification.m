%% Overview

% In their work titled 'Automatic Identification of Surface Defects on Hot-Rolled Steel
% Strip through Scattering Convolution Network,' Ke-Chen Song et al. investigated
% the classification of defects using the NEU surface defect database.

% The authors demonstrated that the best accuracy was attained by employing
% the translation-invariant scattering transform(SCN) for image pre-processing along with
% an SVM classifier.

% These techniques are incorporated into the present code.

%% About the Dataset 
% This dataset contains four kinds of typical surface defects of the hot-rolled
% steel strip, i.e., patches (Pa), pitted surface (PS), inclusion (In) and scratches (Sc).
% The database includes 1200 grayscale 200x200 px images: 300 samples each of four different kinds
% of typical surface defects.

%% Load the Dataset

% For each defect you need a folder should be created, as the labels are taken
% from the folders' names.
DatasetPath = '/MATLAB Drive/Data';
% Use the imageDataStore to read the images.
Imds = imageDatastore(DatasetPath,'IncludeSubfolders',true, 'LabelSource','foldernames');

%% Randomly select and plot 20 images from the dataset in a subplot.
figure;
numImages = 1200;
%rng(100); % Set the seed for repeatability
perm = randperm(numImages,20);
for np = 1:20
    subplot(4,5,np); 
    imshow(Imds.Files{perm(np)});
end
%% Wavelet Image Scattering Feature Extraction 
% The parametters are set based on the article
sf = waveletScattering2('ImageSize',[200 200],'NumRotations',[6 6]);

%% Create the training and testing Dataset
%rng(10); % Set the seed for reproducibility
Imds = shuffle(Imds); % Shuffle the files of the image datataset 
[trainImds,testImds] = splitEachLabel(Imds,0.5); % Allocate 50% of the data(as the previously mentioned paper states), to the training set and hold out the remaining images for testing.
Ttrain = tall(trainImds); % Create tall arrays from the training and test datasets.
Ttest = tall(testImds); % Create tall arrays from the training and test datasets.
trainfeatures = cellfun(@(x)helperScatImages(sf,x),Ttrain,'UniformOutput',false); %helperScatImages obtains the log of the scattering transform feature matrix as well as the mean along both the row and column dimensions of each image. 
testfeatures = cellfun(@(x)helperScatImages(sf,x),Ttest,'UniformOutput',false);

%% Concatenate all the training and test features.
Trainf = gather(trainfeatures);
trainfeatures = (cat(2,Trainf{:}))';
Testf = gather(testfeatures);
testfeatures = (cat(2,Testf{:}))';

%% Create and train the classifier using binary SVMs
classifier = fitcecoc(trainfeatures, trainImds.Labels);

%% Evaluate the Digit Classifier
predictedLabels = predict(classifier, testfeatures);
accuracy = sum(testImds.Labels == predictedLabels)./numel(testImds.Labels)*100
%% Plot the confusion matrix
figure;
confusionchart(testImds.Labels,predictedLabels)
title('Test-Set Confusion Matrix -- Wavelet Scattering')

%% -------------- Appendix — Supporting Functions ---------------------- %

function features = helperScatImages(sf,x)
% This function is only to support examples in the Wavelet Toolbox.
% It may change or be removed in a future release.

% Copyright 2018 MathWorks

smat = featureMatrix(sf,x,'transform','log');
features = mean(mean(smat,2),3);
end
%--------------------------------------------------------------------------


