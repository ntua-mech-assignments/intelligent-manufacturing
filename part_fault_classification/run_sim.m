
clear;
clc;
close all ; 
format compact

DatasetPath = 'data';
Training_percentage = 0.8;
augm_percentage = 0.2;

% Select which processing algorithm to use based on whether a single net or
% multiple binary classifiers are used
process_dataset_single_net(DatasetPath, Training_percentage,augm_percentage);
system('python3 single_classifier.py')

% process_dataset(DatasetPath, Training_percentage,augm_percentage); 
% system('python3 four_classifiers.py')  
