import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd 
import tensorflow as tf 
from sklearn.metrics import  ConfusionMatrixDisplay, confusion_matrix
from sklearn.preprocessing import LabelEncoder
from  sklearn import metrics
import keras_tuner as kt
import os


class myHyperModel(kt.HyperModel):
    def __init__(self, input_shape:tuple):
        self.input_shape = input_shape

    def build(self,hp) : 
        """
        
        Creates a hypermodel for hyperparameter tuning with Keras Tuner

        """
        model = tf.keras.models.Sequential()
        model.add(tf.keras.Input(shape=self.input_shape))
        
        hp_units = hp.Int('units', min_value=8, max_value=64, step=8)
        hp_layers = hp.Int('n_layers',min_value=1, max_value=3, step=1)
        hp_learning_rate = hp.Choice('learning_rate', values=[1e-3, 5e-3, 1e-2])
        # hp_loss_metric = hp.Choice('loss_metric',values=['sparse_categorical_crossentropy', 'kullback_leibler_divergence'])

        for i in range(hp_layers):
            model.add(tf.keras.layers.Dense(units=hp_units, activation='relu',  kernel_initializer='he_uniform'))

        model.add(tf.keras.layers.Dense(4, activation = 'softmax'))

        optimizer = tf.keras.optimizers.Adam(learning_rate=hp_learning_rate)
        model.compile(optimizer=optimizer, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

        return model

def read_normalize_data(X_train_path="features_training.csv", X_test_path="features_testing.csv", y_train_path="labels_training.csv", y_test_path="labels_testing.csv"):
    X_train = pd.read_csv(X_train_path, delimiter=',',header=None, dtype='float64')
    X_test = pd.read_csv(X_test_path, delimiter=',',header=None, dtype='float64')
    y_train_str = pd.read_csv(y_train_path, delimiter=',',header=None, dtype="string")
    y_test_str = pd.read_csv(y_test_path, delimiter=',',header=None, dtype="string")

    # remove any possible NaN values from regionprops failure
    X_train = X_train.dropna() # it is important not to reset the indices !
    X_test = X_test.dropna()
    y_train_str = y_train_str.loc[X_train.index] # use the rows of y that correspond to non-Nan 
    y_test_str = y_test_str.loc[X_test.index]

    y_train = np.zeros((y_train_str.shape[0],1))
    y_test = np.zeros((y_test_str.shape[0],1))

    y_train_str = np.array(y_train_str)
    y_test_str = np.array(y_test_str)

    # Convert labels from strings to categorical
    # Do this separately for the training and the testing set in case one of them does not contain a category (on purpose)
    le = LabelEncoder()
    le.fit(y_train_str.flatten())
    y_train = le.transform(y_train_str.flatten())
    y_train = np.atleast_2d(y_train).transpose()

    y_test = le.transform(y_test_str.flatten())
    y_test = np.atleast_2d(y_test).transpose()

    label_names = list(le.inverse_transform([0,1,2,3]))

    # Normalize training data
    for col in X_train.columns : 
        col_min = X_train[col].min()
        col_max = X_train[col].max()
        X_train[col] = (X_train[col] - col_min) / (col_max - col_min)

    # Normalize testing data
    for col in X_test.columns : 
        col_min = X_test[col].min()
        col_max = X_test[col].max()
        X_test[col] = (X_test[col] - col_min) / (col_max - col_min)
    
    return X_train, X_test, y_train, y_test, label_names


def tune_hyperparams(epochs:int, batch_size=32, validation_split=None):
    """
    
    Performs hyperparameter tuning using the Keras Tuner

    """

    if not os.path.exists("hyperparam_tuning"): 
        os.makedirs("hyperparam_tuning") 

    X_train, X_test, y_train, y_test, label_names = read_normalize_data()

    my_hyper_model = myHyperModel((X_train.shape[1],))

    tuner = kt.Hyperband(my_hyper_model,
                         max_epochs=200,
                         objective='val_accuracy',
                         directory='hyperparam_tuning',
                         project_name='hyerparam_hyperband')
    
    tuner.search(X_train, y_train,epochs=epochs,validation_split=validation_split,batch_size=batch_size)

    # Get the optimal hyperparameters
    best_hps=tuner.get_best_hyperparameters(num_trials=1)[0]

    print(f"""
    The hyperparameter search is complete. The optimal number of units in the densely-connected
    layers is {best_hps.get('units')}, the number of hidden layers is {best_hps.get('n_layers')}, 
    the optimal learning rate for the optimizer is {best_hps.get('learning_rate')}.
    """)

    best_model = tuner.hypermodel.build(best_hps)
    # best_model.fit(X_train,y_train,epochs=200,validation_split=0.2)
    # best_model.evaluate(X_test,y_test)

    return best_model

def build_model() -> tf.keras.Sequential:
    model = tf.keras.Sequential([
        tf.keras.layers.InputLayer(input_shape=(X_train.shape[1],)),
        tf.keras.layers.Dense(64, activation='relu', kernel_initializer='he_uniform'),
        tf.keras.layers.Dense(4, activation='softmax')
    ])

    optimizer_adam = tf.keras.optimizers.Adam(learning_rate=0.005)
    scce_loss = tf.keras.losses.SparseCategoricalCrossentropy()
    poisson_loss = tf.keras.losses.Poisson()
    kl_loss = tf.keras.losses.KLDivergence()

    model.compile(optimizer=optimizer_adam,
                loss=scce_loss,
                metrics=['accuracy'])
    
    return model


def plot_training_progress(history):
    """
    
    Plot the training and validation set accuracy and loss 
    progress
    
    """

    fig, [ax1,ax2] = plt.subplots(1,2, figsize=(9,5))

    ax1.plot(history.history['accuracy'], c='blue')
    ax1.plot(history.history['val_accuracy'], c='red')
    ax1.set_title('model accuracy')
    ax1.set_ylabel('accuracy')
    ax1.set_xlabel('epoch')
    ax1.legend(['train', 'val'], loc='upper left')
    ax1.grid(linestyle='--')

    ax2.plot(history.history['loss'], c='blue')
    ax2.plot(history.history['val_loss'], c='red')
    ax2.set_title('model loss')
    ax2.set_ylabel('loss')
    ax2.set_xlabel('epoch')
    ax2.legend(['train', 'val'], loc='upper left')
    ax2.grid(linestyle='--')
    
    plt.suptitle("Training with Sparse Categorical Cross-Entropy loss")
    plt.show()

if __name__ == "__main__":

    # obtain testing and training data
    X_train, X_test, y_train, y_test, label_names = read_normalize_data()
    
    # build model or tune hyperparameters (comment out)
    model = build_model()
    # model = tune_hyperparams(epochs=150, batch_size=16, validation_split=0.5)

    # train model
    # y_train = tf.one_hot(y_train, len(label_names)) # convert to one-hot if KL or Poission loss is used
    # y_test = tf.one_hot(y_test, len(label_names)) # convert to one-hot if KL or Poisson loss is used 


    history = model.fit(X_train, y_train, epochs=200, batch_size=16, validation_split=0.15, shuffle=True)
    model.evaluate(X_test, y_test)

    # plot training progress
    plot_training_progress(history)

    # Evaluate results
    y_pred = np.argmax(model.predict(X_test), axis=1)
    
    accuracy = metrics.accuracy_score(y_test, y_pred)
    macro_averaged_precision = metrics.precision_score(y_test, y_pred, average = 'macro') # "macro" calculates precision for all classes individually and then averages them
    micro_averaged_precision = metrics.precision_score(y_test, y_pred, average = 'micro') # "micro" calculates class wise true positive and false negative and then uses that to calculate overall precision
    macro_averaged_recall = metrics.recall_score(y_test, y_pred, average = 'macro') # "macro" calculates recall for all classes individually and then averages them
    micro_averaged_recall = metrics.recall_score(y_test, y_pred, average = 'micro') # "micro" calculates class wise true positive and false negative and then uses that to calculate overall recall
    macro_averaged_f1 = metrics.f1_score(y_test, y_pred, average = 'macro') # "macro" calculates f1 score for all classes individually and then averages them
    micro_averaged_f1 = metrics.f1_score(y_test, y_pred, average = 'micro') # "micro" calculates class wise true positive and false negative and then uses that to calculate overall f1 score
    confusion = confusion_matrix(y_test, y_pred)

    # Display evaluation results 
    print("Accuracy is : %lf"%accuracy)
    print("Macro-averaged precision is :%lf"%macro_averaged_precision)
    print("Micro-averaged precision is :%lf"%micro_averaged_precision)
    print("Macro-averaged recall is :%lf"%macro_averaged_recall)
    print("Micro-averaged recall is :%lf"%micro_averaged_recall)
    print("Macro-averaged f1 score is :%lf"%macro_averaged_f1)
    print("Micro-averaged f1 score is :%lf"%micro_averaged_f1)




    disp = ConfusionMatrixDisplay(confusion_matrix=confusion, display_labels=label_names)
    disp.plot(cmap = 'RdYlGn')
    plt.show()
