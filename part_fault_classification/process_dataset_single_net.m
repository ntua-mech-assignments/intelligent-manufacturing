function completion = process_dataset_single_net(DatasetPath, Training_percentage,augmentation_percentage)

%% INPUT VARIABLES
% DatasetPath: Where to find the images dataset
% Training_percentage: How to separate the training and testing dataset
% augmentation_percentage: By how much of the training set percentage to augment the training dataset

%% OUTPUT VARIABLES
% completion: Just a variable to indicate that the image processing has been completed. This procedure should generate 4 .csv files with training and testing dataset and their labels

%% -------------------------------------------------------------------------------------- %%

% rng(13); % deactivate seed to run Monte-Carlo cross validation

%% ---------------------------------- Read images --------------------------------------- %%
Imds = imageDatastore(DatasetPath,'IncludeSubfolders',true, 'LabelSource','foldernames'); % Create a datastore with images paths and labels, done for convenience and cleanliness of the code to reduce error proneness
numImages = numel(Imds.Files); % calculate the how much images are in the dataset
Imds_cell = cell(numImages,1); % create an empty cell to store all the images data, instead of their paths and labels only like in a datastore
Labels = strings(numImages,1); % create an array of empty strings to store the labels of each image

% Iterate to fill the previously mentioned cell and strings array with images 200x200 values for 1200 images and 1200 string labels
for np = 1:(numImages)
    Imds_cell{np,1} = imread(Imds.Files{np}); % Read and store the image in each cell element. Each cell element will contain a matrix of 200x200
    Labels(np) = Imds.Labels(np); % Get labels from the imageDatastore
    fprintf('Read dataset: %.2f%%\n', np/(numImages)*100); % Show the progress as this procedure can take some time
end

%% ---------------------------------- Shuffle Dataset --------------------------------------- %%
n = size(Imds_cell, 1);	% dim = 1 to specify rows, get the number of images
R = randperm(n); % Generate a random permutation of indices of rows of the images cell
Shuffled_Imds_cell = Imds_cell(R, :); % Shuffle the images
Shuffled_Labels = Labels(R, :); % Shuffle the labels accordingly

%% ------------------ Split Dataset to testing and training --------------------------------- %%
v = int64(Training_percentage*n); % Get the number of images thst should be in the training set
X_training = Shuffled_Imds_cell(1:v,:); % Split the images dataset to get the training set
X_testing = Shuffled_Imds_cell(v+1:end,:); % Get the remaining images for the testing set
Y_training = Shuffled_Labels(1:v,:); % Get the according testing set labels
Y_testing = Shuffled_Labels(v+1:end,:); % Get the according training set labels

%% ------------------ Augment Training Dataset --------------------------------- %%

% Training samples are shuffled during training so any "order" that seems
% to be created here is mitigated during training.

% ~~~ 1st augmentation operation - X flip ~~~
ind = randperm(numel(X_training)); % Generate a random permutation of indices of rows of the training images cell
indx = ind(1:int64(numel(ind)*augmentation_percentage));  % Chop the previous dataset to the augmentation percentage, this variable will be an array with the indices of the images that will be added to the testing set to augment it 
labels_aug = strings(numel(indx),1); % Iniatialize an empty strings array to store the augmentation images labels
Imds_cell_aug = cell(numel(indx),1); % Iniatialize an empty cell to store the augmentation images

for i = 1:numel(indx) % Iterate through the augmentation images
    img = X_training{indx(i)}; % Get an augentation image
    img = flip(img, 1); % Mirror the original image to create the augentation image

    labels_aug(i) = Y_training{indx(i)}; % Get the corresponding label for the image
    Imds_cell_aug{i,1} = img; % Add the processed image to the augmention images cell
    fprintf('Augment dataset by %.0f%%: %.2f%%\n', augmentation_percentage*100,100*i/numel(indx)); % Show the progress as this procedure can take some time
end

% ~~~ 2nd augmentation operation - Y flip ~~~
% ind = randperm(numel(X_training));
% indx = ind(1:int64(numel(ind)*augmentation_percentage));
% labels_aug_2 = strings(numel(indx),1);
% Imds_cell_aug_2 = cell(numel(indx),1);
% 
% for i = 1:numel(indx)
%     img = X_training{indx(i)};
%     img = flip(img, 2); 
% 
%     labels_aug_2(i) = Y_training{indx(i)};
%     Imds_cell_aug_2{i,1} = img;
%     fprintf('Augment dataset by %.0f%%: %.2f%%\n', augmentation_percentage*100,100*i/numel(indx));
% end
% 
% Imds_cell_aug = cat(1, Imds_cell_aug, Imds_cell_aug_2); 
% labels_aug = cat(1, labels_aug, labels_aug_2); 

%  ~~~ 3rd augmentation operation - Crop ~~~
% ind = randperm(numel(X_training));
% indx = ind(1:int64(numel(ind)*augmentation_percentage));
% labels_aug_3 = strings(numel(indx),1);
% Imds_cell_aug_3 = cell(numel(indx),1);
% 
% for i = 1:numel(indx)
%     img = X_training{indx(i)};
%     target_size = [150,150];
%     rect = centerCropWindow2d(size(img), target_size);
%     img = imcrop(img, rect);
% 
%     labels_aug_3(i) = Y_training{indx(i)};
%     Imds_cell_aug_3{i,1} = img;
%     fprintf('Augment dataset by %.0f%%: %.2f%%\n', augmentation_percentage*100,100*i/numel(indx));
% end

% ~~~ 4rth augmentation operation - Overall brightness ~~~
% ind = randperm(numel(X_training));
% indx = ind(1:int64(numel(ind)*augmentation_percentage));
% labels_aug_4 = strings(numel(indx),1);
% Imds_cell_aug_4 = cell(numel(indx),1);
% 
% for i = 1:numel(indx)
%     img = X_training{indx(i)};
%     brightness_tuner = randi([-50,50]); 
%     img = img + brightness_tuner ; 
% 
%     labels_aug_4(i) = Y_training{indx(i)};
%     Imds_cell_aug_4{i,1} = img;
%     fprintf('Augment dataset by %.0f%%: %.2f%%\n', augmentation_percentage*100,100*i/numel(indx));
% end
% 
% Imds_cell_aug = cat(1, Imds_cell_aug, Imds_cell_aug_4); 
% labels_aug = cat(1, labels_aug, labels_aug_4); 

% Append augmented images to the original training set
Imds_cell_augmented = cat(1,X_training,Imds_cell_aug); % Concatenate the initial training set images and the augmentation images
Y_augmented = cat(1,Y_training,labels_aug); % Concatenate the corresponding labels

%% ------------------ Testing set processing  --------------------------------- %%

% Testing set 
[~,ncols] = ProcessImage(X_testing{1,1}, 0); % Call the filtering and feature extraction functions, and get the number of features to be extracted for each image
X_test_out = zeros(numel(X_testing),ncols); % Initialize an array to save the augmented testing set features
Y_test_out = Y_testing; % Get the augmented testing set labels
for i = 1: numel(X_testing)
    [X_test_out(i,:), ~] = ProcessImage(X_testing{i,1}, 0); % Call the filtering and feature extraction functions to get the features for each image. The result is a row with ncols
    fprintf('Processing testing dataset: %.2f%%\n', i/ numel(X_testing)*100);
end

% Save to file
writematrix(X_test_out, "features_testing.csv");
writematrix(Y_test_out, "labels_testing.csv");

%% Training set processing 
X_train_out = zeros(numel(Imds_cell_augmented),ncols); % Initialize an array to save the augmented training set features
Y_train_out = Y_augmented; 
for i = 1: numel(Imds_cell_augmented)
    [X_train_out(i,:), ~] = ProcessImage(Imds_cell_augmented{i,1}, 0); % Call the filtering and feature extraction functions to get the features for each image
    fprintf('Processing training dataset: %.2f%%\n', i/ numel(Imds_cell_augmented)*100);
end

% Save to file
writematrix(X_train_out, "features_training.csv");
writematrix(Y_train_out, "labels_training.csv");

completion = "True";

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [x, ncols] = ProcessImage(img, show)
        %
        %   This function wraps the other filtering and feature extraction
        %   functions. It contains as many pairs of filtering-feature
        %   extraction as needed, each pair acting on the original image.
        %
        x = [] ; % members are appended to the feature vector by each pre-processing function
        img_f = FilterImageSc(img, show);
        [x, ncols] = GetImageRegionPropsSc(img_f, x, 0);
        img_f = FilterImageInPa(img, show);
        [x, ncols] = GetImageRegionPropsInPa(img_f, x, 0);
        img_f = FilterImageGeneric(img, show);
        [x, ncols] = GetImageRegionPropsGeneric(img_f, x, 0);

    end

    function [img_out] = FilterImageSc(img_in, show)
        % 
        % This function filters the image so that features of Sc images ar
        % e more easily distinguished.
        %

        img_out = mat2gray(img_in);
        img_out= imflatfield(img_out, 100);
        img_out = histeq(img_out);   
        img_out = imbinarize(img_out, "adaptive"); 
        img_out = imerode(img_out, strel("disk",2)); 
        img_out = imopen(img_out,strel("square",1));

        if show == 1
            h = figure;
            imshowpair(img_in, img_out, "montage", Scaling="none");
            uiwait(h);
        end
    end

    function [img_out] = FilterImageInPa(img_in, show)
        % 
        % This function filters the image so that features of In and Pa are
        % more easily distinguished.
        %
        img_out = mat2gray(img_in);
        img_out = medfilt2(img_out);
        img_out= imflatfield(img_out, 50);
        img_out= imadjust(img_out);
        img_out= imbinarize(img_out);
        img_out = imcomplement(img_out);
        img_out = imopen(img_out, strel("diamond",2)); 

        if show == 1
         h = figure;
         imshowpair(img_in, img_out, "montage", Scaling="none");
         uiwait(h);
        end
    end

    function [img_out] = FilterImageGeneric(img_in, show)
        % 
        % This function is a generic filter that was found to distinguish 
        % some helpful features from all images. It is pretty similar to
        % the filtering function used for In and Pa.
        %
        img_out = mat2gray(img_in);
        img_out = medfilt2(img_out);
        img_out= imflatfield(img_out, 25);
        img_out= imadjust(img_out);
        img_out= imbinarize(img_out);
        img_out = imcomplement(img_out);
        img_out = imopen(img_out, strel("diamond",1)); 
        
        if show == 1
         imshowpair(img_in, img_out, "montage", Scaling="none");
        end
    end

    function [x, ncols] = GetImageRegionPropsSc(img_in, x, show)
        % 
        % This function extracts features that are most prominent in  Sc
        % images.
        %
        props = regionprops("table", img_in, ...
            "MajorAxisLength", ...
            "MinorAxisLength"...
            );

        % find the longest (in terms of major ax) object and get its index 
        [max_major_ax, idx_max_major_ax] = max(props.MajorAxisLength);
        % find the minor ax of this object and determine the aspect ratio
        minor_ax = props.MinorAxisLength(idx_max_major_ax); 
        aspect_ratio = max_major_ax / minor_ax ; % Scratches have large aspect ratios

        n_appends = 2 ; % 2 elements are appended to x
        ncols_prev = length(x); 
        x = [x, max_major_ax, aspect_ratio];
        ncols = length(x);    

        % check whether imageprops did not manage to return anything
        if ncols <= ncols_prev + n_appends 
            x = [x , nan(1, (ncols_prev + n_appends - ncols))]; 
        end 
    end

    function [x, ncols] = GetImageRegionPropsInPa(img_in, x, show)
        % 
        % This function extracts features that are most prominent in In and
        % Pa images.
        %
        props = regionprops("table", img_in, ...
            "Area", ...
            "Perimeter",...
            "MajorAxisLength", ...
            "MinorAxisLength",...
            "MaxFeretProperties", ...
            "MinFeretProperties", ...
            "Orientation"...
            );

        max_area = max(props.Area); % large areas are expected in Pa
        max_perimeter = max(props.Perimeter); % large perimeters are expected in Pa

        % Auxiliary metrics
        [~, idx_max_major_ax] = max(props.MajorAxisLength);
        % find the minor ax of this object and determine the aspect ratio
        minor_ax = props.MinorAxisLength(idx_max_major_ax); 

        % Get the min ferret diameter of the longest (major ax) object. The idea is
        % that this diameter should be perpendicular to the major axis for Sc, 
        % so the orientation calculated below will be close to 90deg
        % Also the  min ferret diameter should be close to the minor axis
        % in the case of Sc.

        min_feret_diameter = props.MinFeretDiameter(idx_max_major_ax);
        min_feret_angle = props.MinFeretAngle(idx_max_major_ax); 
        
        min_feret_to_minor_diff = minor_ax - min_feret_diameter ; % use subtraction to avoid zero-division

        orientation = props.Orientation(idx_max_major_ax);
        straightness = orientation - min_feret_angle ; 

        n_appends = 4 ; % 4 elements are appeded to x
        ncols_prev = length(x); 
        x = [x, max_area, max_perimeter, straightness, min_feret_to_minor_diff];
        ncols = length(x);

        % check whether imageprops did not manage to return anything
        if ncols <= ncols_prev + n_appends 
            x = [x , nan(1, (ncols_prev + n_appends - ncols))]; 
        end 
    end

    function [x,ncols] = GetImageRegionPropsGeneric(img_in, x, show)
        % 
        % This function extracts generic features that were found to
        % increase the classification accuracy.
        %
        props = regionprops("table", img_in, ...
            "Area", ...
            "Perimeter",...
            "Circularity", ...
            "EulerNumber", ...
            "MajorAxisLength", ...
            "MinorAxisLength",...
            "Eccentricity",...
            "Centroid",...
            "Extent");
        
        img_brightness = mean(img_in(:));
        n_regions = height(props);
        mean_area = mean(props.Area);
        std_area = std(props.Area);
        
        std_perimeter = std(props.Perimeter);
        euler = mean(props.EulerNumber);
        std_major_ax = std(props.MajorAxisLength);
        std_minor_ax = std(props.MinorAxisLength);
        mean_ecc = mean(props.Eccentricity);

        n_appends = 9 ; % 9 elements are appeded to x
        ncols_prev = length(x); 
        x = [x, n_regions, img_brightness, mean_area, std_area, ...
            std_major_ax, std_minor_ax, ...
            euler, std_perimeter, mean_ecc];
        ncols = length(x); 

        % check whether imageprops did not manage to return anything
        if ncols <= ncols_prev + n_appends 
            x = [x , nan(1, (ncols_prev + n_appends - ncols))]; 
        end 
    end

end

